#!/usr/bin/env python3

'''Dit programma leest een pileup bestand in. Vervolgens wordt de data geanalyseerd. Het bestand wordt regel voor regel
   gelezen. Zodra een regel een mutatie bevat wordt dit geregistreerd en wordt het aantal mutaties opgehoogd. Nadat het 
   gehele bestand is gelezen worden de mutaties weergegeven waarbij de coverage getoond wordt door het aantal ']' tekens
   en het totaal aantal mutaties.
'''

__author__ = "Kimberley Kalkhoven & Toke Heerkens"

#Imports
import sys
import re

#Make an output file
writing_file = open("output.txt", "w")
total_mismatch = 0
total_match = 0

with open(sys.argv[1]) as opened:
	for line in opened:
		#for every line in the opened file split it
		line = line.split()
		#count the .&, in the  element 4
		dot_komma_amount = sum([line[4].count(x) for x in [",","."]])

		if dot_komma_amount != int(line[3]) or len(line[4]) != int(line[3]):
			#the number of mutations is equal to the diverents between .&,'s plus the total number of matches of the re pattern
			mismatch = ((int(line[3])- dot_komma_amount)+len(re.findall("\+[0-9]+", line[4])))
			
			if mismatch != 0:
			#print you results
				print("{}\tthe missmatchess count is:{} and the matches count is:{}\t{}".format(" ".join(line), mismatch,
																								dot_komma_amount,
																								("]"*int(line[3]))))
				#writing to a file
				writing_file.write("{}\t{}\t{}\t{}\n".format("\t".join(line), mismatch, dot_komma_amount,
															 ("]"*int(line[3]))))
				#make sure your total is correct
				total_match += dot_komma_amount
				total_mismatch += mismatch

# Print the total mismatchs and matces
print("total mismatches:{} and total matches:{}".format(total_mismatch, total_match))
# Close the files you have opened
writing_file.close()

