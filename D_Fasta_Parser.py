#!/usr/bin/env python3

''' Dit programa maakt uit een fastaQ of een Fasta bestand insert statments voor MySQL.'''

__author__ = "Kimberley Kalkhoven & Toke Heerkens"

# Imports
import sys
import re

# File to read in commandline
read_file = sys.argv[1]
open_file = open(read_file)

aantal_A = 0
aantal_C = 0
aantal_T = 0
aantal_G = 0
lengte = 0

mydict = {}

header = ""
sequence = ""
collecting_data_1 = False
collecting_data_2 = False 

for line in open_file:
	line = line.strip()

	if line.startswith(">"):
		mydict[header] = sequence
		header = line
		sequence = ""

	else:
		sequence += line

# ivm met laaste regel om header en sequence niet te missen
mydict[header] = sequence

#For loop maken voor dictionary en dan de lengte van de sequentie en de aantallen berekenen :) 
for keys in sorted(mydict):
	aantal_A = (mydict[keys]).count('A')
	aantal_C = (mydict[keys]).count('C')
	aantal_G = (mydict[keys]).count('G')
	aantal_T = (mydict[keys]).count('T')
	lengte = len((mydict[keys]))
	#Print (keys, 'A', aantal_A,'C', aantal_C,'G', aantal_G,'T', aantal_T, lengte)

	#Start en eind positie bepalen
	for match in re.finditer(">NODE_", keys):
		contignummer = keys[6]
		collecting_data_1 = True
	de_re = "from (?P<complement>complement\()?(?P<start>[0-9]+)\.\.(?P<end>[0-9]+)\)?"

	for match in re.findall(de_re, keys):
		complement = match[0]
		start = int(match[1])
		end = int(match[2])

		if complement == "":
			complement = "+"

		else:
			complement = "-"
		collecting_data_2 = True

	#string formating
	if collecting_data_1 == True and collecting_data_2 == True:
		print("insert into ORF(contig_ID, start_point, end_point, sequence_, strand) values({}, {}, {}, {}, {})".format(contignummer, start, end, mydict[keys], complement))
		collecting_data_1 = False
		collecting_data_2 = False 
