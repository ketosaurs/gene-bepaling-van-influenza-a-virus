#!/usr/bin/env python3

'''
Reads a BLAST XML output file and inserts relevant information into the database

Parsing XML BLAST output using BioPythons NCBI XML parser results in a generator
object, i.e. it will give a new result (blast record) each time the records.next()
function is called. Two use cases are either converting to a list:
blast_record_list = list(blast_records) or using it in a loop (very memory efficient)
as shown below in the process_blast_records() function.

See the NCBIXML
manual for a list of all data elements stored and further usage examples:
http://biopython.org/DIST/docs/tutorial/Tutorial.html#htoc90, chapter 7
'''

# Imports
import sys
from Bio.Blast import NCBIXML
import mysql.connector

__author__ = "Kimberley Kalkhoven & Toke Heerkens"
__version__ = "2016.1"


def read_blast_xml(xml_file_name):
    ''' Uses the BioPython NCBI XML parser
        to create a generator object '''
    result_handle = open(xml_file_name, 'r')
    return NCBIXML.parse(result_handle)

def parse_blast_records(blast_records, cursor, conn):
    ''' function printing information on all BLAST records given in the blast_records generator object.'''
    
    for record in blast_records:
        #The Query sequence name is the ORF name, this can be used to identify the ORF ID in the database.
        for alignment in record.alignments:
            for hsp in alignment.hsps:
                #At this location we have all information available to create a full INSERT query.
                # Insert statement to write database
                query = "INSERT INTO BLAST(ORF_ID, Hit_def, Hsp_gaps, Hit_accession, Hsp_identity, "\
                "Hsp_positive, Hsp_evalue, Hsp_score, Hsp_bit_score, Hsp_query_start, Hsp_query_end,"\
                "Hsp_hit_start, Hsp_hit_end, Hit_len, Hsp_align_len, Hsp_qseq, Hsp_hseq, Hsp_midline) VALUES"\
                "('>{}', '{}', '{}', '{}', '{}', '{}', {}, {}, {},{}, {}, {}, {}, {}, "\
                "{}, '{}', '{}', '{}')".format(record.query, alignment.title.replace("'", "\'"), hsp.gaps, 
                        alignment.accession, hsp.identities, hsp.positives, hsp.expect, hsp.score, hsp.bits, 
                        hsp.query_start, hsp.query_end, hsp.sbjct_start, hsp.sbjct_end, alignment.length, 
                        hsp.align_length, hsp.query, hsp.sbjct, hsp.strand)
                cursor.execute(query)
                conn.commit()
    cursor.close()
    conn.close()

def main():
    '''Main function for script'''
    #Connection to MySQL
    conn = mysql.connector.connect(host=input('Host: '), database=input('Database: '), user=input('User: '), 
                                   password=input('Password: '))
    cursor = conn.cursor()

    # Read XML file
    blast_records = read_blast_xml(sys.argv[1])

    # Process records
    parse_blast_records(blast_records, cursor, conn)

if __name__ == '__main__':
    main()
